import type {Feed} from '../../types'
import {entries, errors, streamState} from '../store'
import {feedToEntries, sortEntries} from '../utils'

/** accept server-side events given by rust-rocket backend */
export default function stream(port: string): Promise<void> {
  let retryTime = 1

  /** abstracted into function to be referenced in backoff/retry sequence
   * if resolved, means all events have been received
   * */
  function connect(): Promise<void> {
    const prefix = port ? `http://localhost:${port}` : ''
    const events = new EventSource(prefix + '/stream')

    events.addEventListener('meta', (msg: MessageEvent<any>) => {
      streamState.set({
        progress: 0,
        total: JSON.parse(msg.data).length
      })
    })

    events.addEventListener('message', (msg) => {
      const fetched = JSON.parse(msg.data) as {feed: Feed, url: string, time: number, err: string}
      if(fetched.err){
        errors.update(prev => [...prev, fetched.err])
        return
      }

      // who cares about perf???
      entries.update((existing) => {
        const newEntries = feedToEntries(fetched.feed).filter(
          (newEntry) =>
            !existing.map((existingEntry) => existingEntry.link).includes(newEntry.link)
        )
        const sorted = sortEntries([...existing, ...newEntries])
        return sorted
      })
      streamState.update((prev) => ({
        ...prev,
        progress: prev.progress + 1
      }))
    })

    events.addEventListener('error', (err) => {
      console.log(err)
      events.close()
      /** exponential backoff */
      const timeout = retryTime
      retryTime = Math.min(64, retryTime * 2)
      console.error(
        `connection lost. attempting to reconnect in ${timeout}s`
      )
      setTimeout(() => connect(), (() => timeout * 1000)())
    })

    const endingPromise = new Promise<void>((resolve) => {
      events.addEventListener('terminate', () => {
        events.close()
        resolve()
      })
    })

    return endingPromise
  }

  return connect()
}
