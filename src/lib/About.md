# About Velvet

## what is it

This is Velvet, a news feed and blog post aggregator, a feed reader
for my friends.^[More concretely, Atom or RSS XML feeds, which often
happen to be news feeds and blog posts. I've considered write parsers
for Twitter feeds and Youtube channels and so on, because that would
be cool, but I have more fondness for non-proprietary protocols than
trying to deconstruct social media markup that could change at a
moment's notice based on the whims of a multibillion-dollar
corporation. So I probably won't.] It depends on some of my hardware
that I'm sharing with specific people, and it's designed to be
personal and relational. It has some principles for more ethical and
principled ways to make websites. For a better Web. For my friends!

The feed at first just contains my own feed, but you can customize it
to show whatever feeds you like.

## how is it

### the UI

The stuff on the screen is made through the Svelte JavaScript
framework. I like Svelte a lot for its compilation approach, avoiding
the need for a virtual DOM, reducing bundle size by doing all of its
work at build time, and giving it a fairly generous scope because
additional features result in no additional bundle size.^[Of course,
additional features can still result in a bloated and unmaintainable
library. I just meant that Svelte freely tackles things like
animations that other frameworks would leave out for bundle size
constraints.]

The UI renders twenty elements at first, and renders more elements as
the user scrolls down. This occurs through the `IntersectionObserver`
API.

I also have a small build step that converts this text from markdown
to HTML using `marked.js`. It's just way easier to write in markdown
for me, especially for things like footnotes and appropriately handled
links.

### servers and CORS

First, a basic note on CORS. CORS, or _cross-origin resource sharing_,
is a security policy meant to prevent some sites from maliciously
using information from or posing as another site and eventually lure
users into handing over private data. Browsers limit websites to only
have access to resources from their own domain, unless a resource from
another domain explicitly configures the
_Access-Control-Allow-Methods, Access-Control-Allow-Headers_, and
_Access-Control-Allow-Origin_ headers on their resources.

CORS makes feeds like Velvet a bit difficult. The Velvet website can't
actually fetch information from any blog, to see the latest posts or
even meta information like "last published" and the like. That would
require requesting information from another website, and is blocked
under CORS.

The general solution is to delegate the actual reading of the feed to
a server, do a tiny bit of parsing to make it easier to display, and
pass that result from the server to the client. Here's a chart about
it:

How do you set up the server? Most companies nowadays use a cloud
service like AWS or Heroku. They can scale (give more computing
resources) on demand, can shut down and immediately fire up when
needed, and let you avoid worrying about hardware. Other companies buy
dedicated server equipment and handle that themselves, fronting some
of the up-front costs in favor of avoiding monthly payments.

My server isn't like either of those; it's just run out of an old
laptop, a $600 Lenovo Thinkpad that I used for a few years before I
broke it (and later fixed). It's sitting at my parents' home in
Philadelphia, plugged into a wall, in a discreet corner, behind a
curtain. The content you're seeing is just a connection to that
laptop.

### The UI meets the server

I use the Vite development framework, which handles things like hot
reloading, as well as the Svelte compilation, minification,
transpilation, and bundling at the build step.^[A glossary for those
unfamiliar. **Hot reloading** lets you see a change in your browser as
soon as you make a change to your code, without needing to restart a
local development server or manually reload a page. **Minification**
is the transformation of code to take up as little disk space as
possible, involving the elimination of whitespace, cutting down
variable names, . This helps your website load faster, because less
code needs to be downloaded by the client. **Bundling** is the
assemblage of all JavaScript files into a smaller set of files. The
exact number depends on your build setup; you generally want at most
one file per route, so that you can have few network requests but also
minimize the actual amount of code loaded for a page.] Vite places the
products from the build step in the `server/build/` folder.

In the `server/` folder, I have running a Rocket web application that
does three things.^[Rocket is a web application ] It responds to `GET`
requests for items in the `server/build` folder, which essentially
lets the user view the website as bundled by Vite.

But the true reason I want a server is because I need an RSS fetcher
on the server side. So the Rocket application responds to `GET`
requests for the latest feed, and it responds to `POST` requests for
modifications to the feed or a demand to update the feed when the user
presses `refresh`. This API is essentially the bridge

## why is it

The TLDR: even in mundane and even uninteresting situations and apps,
we can choose our way to a better Web.

When I began this project, it was just a fun distraction to learn
about Svelte and make something pretty. But as I worked more and more
on the project, I realized that even in this very mundane and
uninteresting premise, there were choices involved that I could think
through. They break down across three main categories.

### premise: the opposite of doomscrolling

Normally when we think of "feed," we think of endlessly scrolling down
Twitter or Tiktok or Instagram or Instagram Reels or Youtube Shorts or
... anything else.^[But you're kind of weird if you do this on
Facebook or (god forbid) LinkedIn. Just my opinion :)] The stream of
content never ends, and it's in companies' best interests to keep you
there for as long as possible, providing short bursts of entertainment
on demand to keep us coming back to the app.

Velvet is a feed too, but it has a much different premise. Content is
a bit longer, not always entertaining, curated by yourself only and no
algorithm except "most recent at the top." There are probably a few
new articles every time you get around to checking it, but the stream
of content ends pretty quickly, giving you nothing to do but go on
with your life. And there's nothing about this that can make you come
back later; it's totally, completely within your right to take a look
once, decide it's boring, and never come back to this.

Feed readers like this used to be a lot more common.^[At least that's
what I've been led to believe; in their heyday I still only visited
CoolMathGames, Poptropica, and Club Penguin, and not much else]

### design: simple

This is a one person project made by an amateur. I have the choice to
choose a fancy layout with more animations, render formatted previews
of the articles, show picture previews that the articles have, and so
on; or I can just do things simply.

I chose the latter. I chose Lato, which I think is the ultimate
readable font; I chose a simple layout for each card and a
high-contrast site-specific background, I worked to make typography
large and in a hierarchy to imply structure. I worked to make the app
look quiet.

Partly this was because I think I'm just one person, and of course
it's better to do an easy task well than a hard task poorly. Partly it
was because of premise: the feed reader is mundane if anything else,
it's not special, it should be a useful everyday tool and not pretend
to be anything else. And partly it was because of principle: focusing
on readability means less people are excluded, and focusing on
simplicity means that the tool is relatively easy to understand.

### architecture: democratic

But the most meaningful point to me is in the app's architecture, a
system that the user really doesn't see or probably care about. I
could have hosted this on AWS or Heroku, which I've had good
experiences with before, but I chose to do this on my own laptop
sitting at home, and share resources as I have them.

A tangent on two fundamental processes in capitalism. One is
_accumulation_, or gathering more profit, then expanding one's means
of production, then reaping more profit, repeat. Another is _commodity
fetishism_, or the recognition of a relation between people as a
relation between things or people and things.

Modern tech stacks adhere very closely

## misc.

- **huh?** If you want more information, you can take a look at the
  Gitlab repository's codebase or commit history. That probably will
  leave you only more confused if you come from a non-technical
  standpoint, so if you're more confused you can DM me on Twitter!
- **the name?** Velvet is a product made with S**vel**te, the UI
  framework, so I thought the words were close enough to warrant the
  name. Both words mean something like "smooth," _svelte_ beng an
  adjective and _velvet_ being a noun or something made to be
  smooth.^[The two actually appear to be etymologically related, but
  only at the Proto-Indo European level? See
  [svelte](https://www.etymonline.com/word/svelte) and
  [velvet](https://www.etymonline.com/word/velvet).] The app is
  definitely intended to have a very smooth feel; I'd like it to just
  _work_, and work on any bandwidth, and across any machine, and to
  have transitions, and have those transitoins be 60 frames per
  second. That seems like a buttery smooth app to me, a goal of
  something without hiccups or technical imperfections if only because
  it is so simple and small in scope. But the name is just a name, I
  mostly picked it because it sounded good.
- **why do you care so much about this??** I don't knowwwww it's just
  fun for me to sit in a room by myself and do shit like this, for
  some reason my brain loves to think about it even if it's not
  epistemologically interesting or anything
