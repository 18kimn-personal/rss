import {writable} from 'svelte/store'
import type {Color, Entry} from '../types'

const cache = JSON.parse(
  localStorage.getItem('entries')
) as Entry[]
const cachedColors =
  JSON.parse(localStorage.getItem('colors')) || ([] as Color[])

interface StreamState {
  progress: number
  total: number
}

export const streamState = writable({} as StreamState)
export const errors = writable([] as string[])
export const lastFetchedDate = writable(
  new Date(localStorage.getItem('lastFetchedDate'))
)
export const entries = writable(cache || ([] as Entry[]))
export const filtered = writable(cache || ([] as Entry[]))
export const colors = writable(cachedColors)

/* state management */
export const simple = writable(false)
