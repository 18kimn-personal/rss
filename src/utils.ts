import {colors} from './store'
import type {Feed, Entry, Color} from '../types'

/** on fetch, gets dates the right way */
export function feedToEntries(fetched: Feed) {
  const entries = fetched.items.map((item) => ({
    ...item,
    pub_date: new Date(item.pub_date)
  }))
  return entries
}

/** sorts by date */
export function sortEntries(entries: Entry[]): Entry[] {
  return entries
    .map((entry) => ({
      ...entry,
      pub_date:
        typeof entry.pub_date === 'string'
          ? new Date(entry.pub_date)
          : entry.pub_date
    }))
    .sort((a, b) => Number(b.pub_date) - Number(a.pub_date))
}

/** turns HTML strings from RSS feeds into safe text content */
export function stripHTML(html: string) {
  const doc = new DOMParser().parseFromString(html, 'text/html')
  const text = doc.body.textContent || ''
  return text.slice(0, 350) + ' ...'
}

/** looks for links and renders them as such; should be used after
 * stripHTML to end up with links-only markup */
export function makeLinks(text: string) {
  const regex = /(https?:\/\/[^\s]+)/g
  return text.replace(regex, '<a href="$1">$1</a>')
}

/** returns a light random color */
export function randomColor() {
  return 'hsl(' + Math.random() * 360 + ', 100%, 95%)'
}

/** tricky code dedup */
export function updateColors(
  entries: Entry[],
  existingColors: Color[]
) {
  const computedColors: Color[] = entries
    .reduce((acc, curr) => {
      const filtered = acc.filter(
        (item) => item.source !== curr.meta?.title
      )
      return [
        ...filtered,
        {source: curr.meta?.title, color: randomColor()}
      ]
    }, [] as Color[])
    .filter(
      (scheme) =>
        !existingColors
          .map((color) => color.source)
          .includes(scheme.source)
    )

  const newColors = [...existingColors, ...computedColors]
  colors.set(newColors)
  localStorage.setItem('colors', JSON.stringify(newColors))
}
