# Velvet: an RSS feed reader

This is a small RSS reader I wrote to procrastinate from some other
duties. It works moderately well, so feel free to use it as well.

The parser is written through `xml2js`, and was mostly through
trial-and-error, so there's no guarantees :') The frontend is built
through Svelte.

To use this, clone the repository, and then from inside of it run
`pm2 start "pnpm start"`. Pm2 is a daemonizer that can keep this
perpetually running in the background, and pnpm is a tool for managing
JavaScript projects that I like. This will start the server on port
4200 and the UI on port 4300; this can be configured through the
`.env` file.

Feeds are stored in the `server/feeds` file; just paste in a URL on a
new line.
