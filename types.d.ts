export interface Meta {
  title: string
  link: string
  description: string
  last_build_date: string
}

export interface Entry {
  title: string
  link: string
  pub_date: Date
  description: string
  author: string
  meta: Meta
}

export interface Feed {
  /* these four fields are just Meta ... how to destructure? */
  title: string
  link: string
  description: string
  last_build_date: string
  items: Entry[]
}

export interface Color {
  source: string
  color: string
}
