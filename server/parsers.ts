import {Builder} from 'xml2js'
import type {Entry, Feed, Meta} from '../types'

const builder = new Builder()

interface RawXML {
  title: string
  [prop: string]: any
}

/** one parser for traditional RSS feeds */
export function parseRSS(obj: RawXML): Feed {
  const {title, description, lastBuildDate, link, item} =
    obj.rss.channel
  const meta = {
    title,
    link,
    description,
    last_build_date: lastBuildDate
  }
  return {
    ...meta,
    items:
      item && Array.isArray(item)
        ? item.map((item) => {
          const {title, link, pubDate} = item
          const author = item.author
            ? item.author
            : item['dc:creator']
          const description = item['content:encoded']
            ? item['content:encoded']
            : item.description
          const entry: Entry = {
            title,
            description,
            link,
            pub_date: pubDate,
            author,
            meta
          }
          return entry
        })
        : []
  } as Feed
}

/** and one parser for the supposedly richer atom feed */
export function parseAtom(obj: RawXML): Feed {
  const {title, subtitle, link, updated, entry} = obj.feed
  const meta: Meta = {
    title: typeof title === 'string' ? title : title._,
    link: link['$']?.href,
    description: subtitle,
    last_build_date: updated
  }
  return {
    ...meta,
    items: entry.map((item) => {
      const {title, content, link, published, updated, author} = item
      const description = content && builder.buildObject(content)
      const cleanLink = Array.isArray(link)
        ? link[0]['$'].href
        : link['$'].href
      const result: Entry = {
        title: typeof title === 'string' ? title : title._,
        description,
        link: cleanLink,
        pub_date: published || updated,
        author,
        meta
      }
      return result
    })
  }
}
