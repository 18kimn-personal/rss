import {promises as fs} from 'fs'
import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import refresh from './refresh'
import {resolve, join, dirname} from 'path'
import {fileURLToPath} from 'url'

const __dirname = dirname(resolve(fileURLToPath(import.meta.url)))
const app = express()

app.use(cors()).use(express.static(join(__dirname, 'build'))).use(bodyParser.text())

app.get('/feed', async (_, res) => {
  const feedPath = resolve(__dirname, '../feed.json')
  res.sendFile(feedPath)
})

app.get('/sources', async (_, res) => {
  const feedPath = resolve(__dirname, '../feeds')
  res.sendFile(feedPath)
})

app.post('/sources', async (req, res) => {
  const feedPath = resolve(__dirname, '../feeds')
  fs.writeFile(feedPath, req.body)
  res.end()
})

app.get('/stream', async function (_, res) {
  res.set({
    'Cache-Control': 'no-cache',
    'Content-Type': 'text/event-stream',
    'Connection': 'keep-alive'
  })
  res.flushHeaders()

  const feedPromises = await refresh()

  res.write('event: meta\n')
  res.write(
    `data: ${JSON.stringify({length: feedPromises.length})}\n\n`
  )

  res.write('event: message\n')
  const sentPromises = feedPromises.map(async (feedPromise) => {
    const fetched = await feedPromise.catch((err) => err)
    res.write(`data: ${JSON.stringify(fetched)}\n\n`)
    return fetched
  })

  const feeds = await Promise.all(sentPromises)
  const feedPath = resolve(__dirname, '../feed.json')
  fs.writeFile(feedPath, JSON.stringify(feeds))

  res.write('event: terminate\n')
  res.write('data: anything\n\n')
  res.end()

  return
})

app.post('/', async (_, res) => {
  const feed = await refresh()
  res.send(feed)
  const feedPath = resolve(__dirname, '../feed.json')
  fs.writeFile(feedPath, JSON.stringify(feed))
})

app.listen(4300)
