import {promises as fs} from 'fs'
import {dirname} from 'path'
import type {Feed} from '../types'
import {fileURLToPath} from 'url'
import {parseStringPromise} from 'xml2js'
import {parseRSS, parseAtom} from './parsers'

const __dirname = dirname(fileURLToPath(import.meta.url))
process.chdir(__dirname)

type Result = {
  feed?: Feed
  url: string
  time?: number
  err?: string
}

/** requires node 18 for native fetch ! */
/** fetches feeds and parses Atom and vanilla RSS feeds into a common
 * structure */
async function processXML(
  url: string,
  time: Date
): Promise<Result> {
  const fetched = await fetch(url)
    .then((res) => res.text())
    .catch((err) => err)
  if (fetched instanceof Error) {
    throw fetched
  }

  const result = await parseStringPromise(fetched, {
    explicitArray: false
  }).catch((err) => err)
  if (result instanceof Error) {
    throw result
  }

  return {
    feed: result.rss ? parseRSS(result) : parseAtom(result),
    url: url,
    time: Number(new Date()) - Number(time) // for benchmarking
  }
}

/** so we don't have to wait for results */
async function logResults(results: Promise<Result>[]) {
  // log meta to console
  const meta = (await Promise.all(results)).map((result) => ({
    url: result.url,
    time: result.time,
    err: result.err
  }))

  console.table(meta)
}

/** final wrapper to call the above logic for every blog */
async function refresh(): Promise<Promise<Result>[]> {
  const time = new Date()
  const sources = await fs.readFile('../feeds', 'utf-8')
  const fetched: Promise<Result>[] = sources
    .split('\n')
    .filter((content) => content)
    .map((url) =>
      processXML(url, time).catch((err) => ({
        url,
        err: `Error at site ${url}: ${err}`
      }))
    )

  // log as a side effect (i.e. non-blocking
  logResults(fetched)

  // and return feeds as the rest
  return fetched
}

export default refresh
